nresp = packet_get_int();
if (nresp > 0) {
    response = xmalloc(nresp * sizeof(char*));
    for (i = 0; i < nresp; i++)
        response[i] = packet_get_string(NULL);
}

//From "Shellcoder's Handbook"
//In this case, nresp is an integer directly out of an SSH packet. It is multiplied
//by the size of a character pointer, in this case 4, and that size is allocated as the
//destination buffer. If nresp contains a value greater than 0x3fffffff, this multiplication will exceed 
//the maximum value for an unsigned integer and overflow. It is possible to cause a very small memory allocation 
//and copy a large number of character pointers into it. 