//OpenBSD ftpd off-by-one

char npath[MAXPATHLEN];
int i;
for (i = 0; *name != '\0' && i < sizeof(npath) - 1; i++, name++) {
    npath[i] = *name;
    if (*name == '"')
        npath[++i] = '"';
}
npath[i] = '\0';

//Although this function tryes to be safe leaving space for null-byte in a loop, 
//if the last character is a quote we get off-by-one